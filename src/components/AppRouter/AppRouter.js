import './AppRouter.css';
import React from 'react';
import UserPage from '../UserPage/UserPage';
import Login from '../Login/Login';
import { Switch, Route, withRouter, Redirect } from 'react-router-dom';
import PrivateRoute from '../PrivateRoute/PrivateRoute';

export const AppRouter = () => {
  return (
    <div className="App">
      <Switch>
        <PrivateRoute component={UserPage} exact path="/users/me" />
        <PrivateRoute component={UserPage} path="/users/:name" />
        <Route path="/login" component={Login} />
        <Redirect to="/users/me" />
      </Switch>
    </div>
  );
};

export default withRouter(AppRouter);
