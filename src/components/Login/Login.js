import React, { Component } from 'react';
import { connect } from 'react-redux';
import { authorize, getIsAuthorized } from '../../ducks';
import { Redirect } from 'react-router-dom';
import { getError } from '../../ducks/users';

class Login extends Component {
  handleEnterPress = (e) => {
    if (e.key === 'Enter' && e.target.value) {
      const { value } = e.target;
      this.props.authorize(value);
    }
  };

  render() {
    const { isAuthorized, error } = this.props;

    if (isAuthorized) {
      return <Redirect to="/users/me" />;
    }

    return (
      <div className="login">
        <h1>Введите свой токен</h1>
        <input
          name="login"
          type="text"
          placeholder="your token"
          onKeyPress={this.handleEnterPress}
        />
        <p className="error">{error}</p>
      </div>
    );
  }
}

const mapDispatchToProps = { authorize };

const mapStateToProps = (state) => ({
  isAuthorized: getIsAuthorized(state),
  error: getError(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
