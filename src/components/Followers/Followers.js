import './Followers.css';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { followersRequest, getFollowers } from '../../ducks';

const Followers = (props) => {
  const { login, followersRequest, followers } = props;

  useEffect(() => {
    followersRequest(login);
  }, [login]);

  return (
    <div className="followers">
      {followers.map((follower, index) => (
        <div key={index} className="follower">
          <Link to={`/users/${follower.login}`}>
            <img className="avatar-follower" src={follower.avatar_url} />
            <div>{follower.login}</div>
          </Link>
        </div>
      ))}
    </div>
  );
};

const mapDispatchToProps = { followersRequest };
const mapStateToProps = (state) => ({ followers: getFollowers(state) });

export default connect(mapStateToProps, mapDispatchToProps)(Followers);
