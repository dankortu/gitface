import './UserPage.css';
import React, { useEffect } from 'react';
import Spinner from 'react-svg-spinner';
import { connect } from 'react-redux';
import {
  getError,
  getIsFetching,
  userRequest,
  clearLoginError,
  getLoginError,
} from '../../ducks/users';
import { followersRequest } from '../../ducks/followers';
import { logout } from '../../ducks/auth';
import { getIsTokenOwner } from '../../ducks/users';
import Followers from '../Followers/Followers';
import { Link } from 'react-router-dom';

const UserPage = (props) => {
  const {
    user: { login, avatar, followersNumber },
    match: {
      params: { name },
    },
    userRequest,
    followersRequest,
    loginError,
    isFetching,
  } = props;
  // componentDidMount() {
  //   const {
  //     userRequest,
  //     match: {
  //       params: { name },
  //     },
  //   } = this.props;
  //   name ? userRequest(name) : userRequest();
  // }

  // componentDidUpdate(prevProps) {
  //   const {
  //     userRequest,
  //     followersRequest,
  //     match: {
  //       params: { name },
  //     },
  //   } = this.props;
  //   if (name !== prevProps.match.params.name) {
  //     userRequest(name);
  //     followersRequest(name);
  //   }
  // }
  useEffect(() => {
    userRequest(name);
  }, [name]);

  if (isFetching) {
    return <Spinner size="64px" color="fuchsia" gap={5} />;
  }

  return (
    <div className="user-page">
      <div className="control-panel">
        <button onClick={props.logout} className="button">
          LOGOUT
        </button>
        <Link to="/users/me" className="button">
          МОЙ ПРОФИЛЬ
        </Link>
      </div>
      {loginError ? (
        <div>НЕВЕРНЫЙ АДРЕСС ПОЛЬЗОВАТЕЛЯ</div>
      ) : (
        <div className="current-user">
          <h1>{login}</h1>
          <img src={avatar} alt="avatar" className="avatar-user" />
          <p>{`FOLLOWERS: ${followersNumber}`}</p>
          <Followers login={login} />
        </div>
      )}
    </div>
  );
};

const mapDispatchToProps = {
  userRequest,
  logout,
  followersRequest,
  clearLoginError,
};
const mapStateToProps = (state) => ({
  user: getIsTokenOwner(state),
  error: getError(state),
  loginError: getLoginError(state),
  isFetching: getIsFetching(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserPage);
