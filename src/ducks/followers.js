import { combineReducers } from 'redux';
import { handleActions, createActions } from 'redux-actions';
import { logout, authorize } from './auth';

export const {
  followersRequest,
  followersSuccess,
  followersFailure,
} = createActions(
  'FOLLOWERS_REQUEST',
  'FOLLOWERS_SUCCESS',
  'FOLLOWERS_FAILURE'
);

const followers = handleActions(
  {
    [followersSuccess]: (state, action) => action.payload,
    [logout]: () => [],
  },
  []
);

const errorFollowers = handleActions(
  {
    [followersFailure]: (state, action) => action.payload,
    [authorize]: () => '',
  },
  ''
);

export const getFollowers = (state) => state.followers.followers;
export const getErrorFollowers = (state) => state.followers.errorFollowers;
export default combineReducers({ errorFollowers, followers });
