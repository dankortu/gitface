import { combineReducers } from 'redux';
import auth from './auth';
import users from './users';
import followers from './followers';

export { authorize, logout, getIsAuthorized } from './auth';
export { getIsTokenOwner, getIsFetching, getError } from './users';
export { getFollowers, getErrorFollowers, followersRequest } from './followers';
export default combineReducers({
  auth,
  users,
  followers,
});
