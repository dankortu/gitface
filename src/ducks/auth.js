import { handleActions, createActions } from 'redux-actions';

export const { authorize, logout } = createActions('AUTHORIZE', 'LOGOUT');

export const getIsAuthorized = (state) => state.auth.isAuthorized;

const auth = handleActions(
  {
    [authorize]: (state) => ({
      ...state,
      isAuthorized: true,
    }),
    [logout]: (state) => ({
      ...state,
      isAuthorized: false,
    }),
  },
  {
    isAuthorized: false,
  }
);

export default auth;
