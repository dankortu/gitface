import { combineReducers } from 'redux';
import { handleActions, createActions } from 'redux-actions';
import { logout, authorize } from './auth';

export const {
  userRequest,
  userSuccess,
  userFailure,
  clearLoginError,
  setLoginError,
} = createActions(
  'USER_REQUEST',
  'USER_SUCCESS',
  'USER_FAILURE',
  'CLEAR_LOGIN_ERROR',
  'SET_LOGIN_ERROR'
);

const isFetching = handleActions(
  {
    [authorize]: () => true,
    [userRequest]: () => true,
    [userFailure]: () => false,
    [userSuccess]: () => false,
    [setLoginError]: () => false,
  },
  false
);

const tokenOwner = handleActions(
  {
    [userSuccess]: (state, action) => action.payload,
    [logout]: () => ({}),
  },
  {}
);
const loginError = handleActions(
  {
    [setLoginError]: (state, action) => action.payload,
    [clearLoginError]: () => null,
  },
  null
);
const error = handleActions(
  {
    [userFailure]: (state, action) => action.payload,
    [authorize]: () => null,
  },
  null
);

export const getIsTokenOwner = (state) => ({
  login: state.users.tokenOwner.login,
  avatar: state.users.tokenOwner.avatar_url,
  followersNumber: state.users.tokenOwner.followers,
});

export const getIsFetching = (state) => state.users.isFetching;
export const getError = (state) => state.users.error;
export const getLoginError = (state) => state.users.loginError;

export default combineReducers({ isFetching, tokenOwner, error, loginError });
