import { fork } from 'redux-saga/effects';
import userRequestWatch from './users';
import { authFlow } from './auth';
import userFollowersListWatch from './followers';

export default function* () {
  yield fork(authFlow);
  yield fork(userRequestWatch);
  yield fork(userFollowersListWatch);
}
