import { call, put, takeLatest } from 'redux-saga/effects';
import { getUserFollowers } from '../api';
import {
  followersRequest,
  followersSuccess,
  followersFailure,
} from '../ducks/followers';

function* userFollowersList(action) {
  try {
    const followers = yield call(getUserFollowers, action.payload);
    yield put(followersSuccess(followers.data));
  } catch (error) {
    yield put(followersFailure(error.response.status));
  }
}

function* followersRequestWatch() {
  yield takeLatest(followersRequest, userFollowersList);
}

export default followersRequestWatch;
