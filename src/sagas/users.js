import { call, put, takeLatest } from 'redux-saga/effects';
import { getUserInformation } from '../api';
import { logout } from '../ducks';
import {
  userRequest,
  userSuccess,
  userFailure,
  setLoginError,
  clearLoginError,
} from '../ducks/users';

function* userData(action) {
  yield put(clearLoginError());
  try {
    const user = yield call(getUserInformation, action.payload);
    yield put(userSuccess(user.data));
  } catch (error) {
    const status = error.response.status;
    const message = error.message;

    if (status !== 404) {
      yield put(userFailure(message));
      yield put(logout());
    } else {
      yield put(setLoginError(status));
    }
  }
}

function* userRequestWatch() {
  yield takeLatest(userRequest, userData);
}

export default userRequestWatch;
