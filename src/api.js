import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://api.github.com/',
});

export const setTokenApi = (access_token) =>
  (instance.defaults.params = { access_token });

export const clearTokenApi = () =>
  (instance.defaults.params = { access_token: undefined });

export const getUserInformation = (login) =>
  instance(login ? `users/${login}` : 'user');

export const getUserFollowers = (login) =>
  instance(`users/${login}/followers?pages=1&per_page=100`);
